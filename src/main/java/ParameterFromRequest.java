import javax.servlet.http.HttpServletRequest;

public class ParameterFromRequest {

    final private HttpServletRequest req;

    public ParameterFromRequest(HttpServletRequest req) {
        this.req = req;
    }

    public double getDouble(String name){
        if (req.getParameter(name) == null && req.getParameter(name).length() == 0){
            throw new IllegalStateException(String.format("Parameter %s is missing", name));
        } else {
            return Double.parseDouble(req.getParameter(name));
        }
    }

    public String getString(String name){
        if (req.getParameter(name) == null && req.getParameter(name).length() == 0){
            throw new IllegalStateException(String.format("Parameter %s is missing", name));
        }   else{
            return req.getParameter(name);
        }
    }
}
