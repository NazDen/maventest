import Calculator.CommandCalc;
import Calculator.Commands;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.List;

public class ServletCalculator extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Files.copy(Paths.get("form_calc.html"), resp.getOutputStream());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DecimalFormat decimalFormat = new DecimalFormat("#.#########");
        resp.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_8));
        PrintWriter writer = resp.getWriter();
        ParameterFromRequest pfr = new ParameterFromRequest(req);
        try {
            double x = pfr.getDouble("x");
            double y = pfr.getDouble("y");
            String cmdName = pfr.getString("op");
            List<CommandCalc> commandCalcs = Commands.cmdList();
            commandCalcs.stream()

                    .filter(c -> c.toText().equalsIgnoreCase(cmdName))
                    .forEach(c -> writer.printf(String.format("%s %s %s = %s", decimalFormat.format(x), c.op(), decimalFormat.format(y), decimalFormat.format(c.doCmd(x, y)))));
        } catch (ArithmeticException e) {
            writer.print(e.getMessage());
        } catch (NumberFormatException e) {
            writer.print("Wrong parameter");
        } catch (RuntimeException e) {
            writer.print(e.getMessage());
        }
    }
}

//        } catch (ArithmeticException e){
//            responseMessage = "You can't divide by zero";
//        } catch (NumberFormatException e){
//            responseMessage = "Integer conversion error";
//        } catch (RuntimeException e){
//        responseMessage = e.getMessage();
//        } finally {
//            PrintWriter writer = resp.getWriter();
//            writer.println(responseMessage);
//        }


