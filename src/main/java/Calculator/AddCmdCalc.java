package Calculator;

public class AddCmdCalc implements CommandCalc {
    final private String name = "add";
    final private String symbol = "+";

    @Override
    public String toText() {
        return name;
    }

    @Override
    public String op() {
        return symbol;
    }

    @Override
    public double doCmd(double x, double y) {
        return x+y;
    }
}
