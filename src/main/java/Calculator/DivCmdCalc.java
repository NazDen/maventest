package Calculator;

public class DivCmdCalc implements CommandCalc {
    final private String name = "div";
    final private String symbol = "/";

    @Override
    public String toText() {
        return name;
    }

    @Override
    public String op() {
        return symbol;
    }

    @Override
    public double doCmd(double x, double y) {
        if (y == 0){
            throw new ArithmeticException("You can't divide by zero");
        }
        else {
            return x/y;
        }
    }
}
