package Calculator;

import Calculator.CommandCalc;

public class MulCmdCalc implements CommandCalc {
    final private String name = "mul";
    final private String symbol = "*";

    @Override
    public String toText() {
        return name;
    }

    @Override
    public String op() {
        return symbol;
    }

    @Override
    public double doCmd(double x, double y) {
        return x*y;
    }
}
