package Calculator;

public interface CommandCalc {

    String toText();
    String op();
    double doCmd(double x, double y);
}
