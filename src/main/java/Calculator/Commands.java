package Calculator;

import java.util.ArrayList;
import java.util.List;


public class Commands {
    public  static List<CommandCalc> cmdList(){
        List<CommandCalc> commandCalcs = new ArrayList<>();
        commandCalcs.add(new AddCmdCalc());
        commandCalcs.add(new SubCmbCalc());
        commandCalcs.add(new MulCmdCalc());
        commandCalcs.add((new DivCmdCalc()));
    return commandCalcs;
    }

}
